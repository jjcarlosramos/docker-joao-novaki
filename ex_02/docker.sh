docker volume create --name sharedVolume
docker run -d --name=container1 -v sharedVolume:/sharedvolume nginx:latest
docker run -d --name=container2 --volumes-from container1 nginx:latest
